# layui-iconExtend

#### 介绍
基于layui的一款扩展插件，可以通过此插件可以对layui图标进行扩展，并提供基础图标操作方法。

#### 安装教程
下载发行版或直接拉取源码即可

#### 部分实现效果展示（动态控制）
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/xg.gif)
#### 使用说明
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/1.png)
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/2.png)
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/3.png)
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/4.png)
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/5.png)
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/6.png)
![img](https://gitee.com/zhiqiang94/layui-icon-extend/raw/master/layui_exts/iconExtend/%E7%A4%BA%E4%BE%8B/7.png)